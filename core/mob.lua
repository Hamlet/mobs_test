--[[
	Mobs Test - Test mob for Mobs Redo.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Mobile entity
--

mobs:register_mob('mobs_test:test', {
	nametag = 'Test Mob',
	type = 'npc',
	hp_min = minetest.PLAYER_MAX_HP_DEFAULT,
	hp_max = minetest.PLAYER_MAX_HP_DEFAULT,
	armor = 100,					-- Same as the player
	walk_velocity = 1,				-- Nodes per second
	run_velocity = 5.2,				-- Same as "sprint mod" (nodes per second)
	stand_chance = 50,				-- 50%
	walk_chance = 50,				-- 50%
	randomly_turn = true,
	jump = true,
	jump_height = 1.1,				-- Nodes
	stepheight = 1.1,
	pushable = true,
	view_range = 16,				-- Nodes (one mapblock)
	damage = 1,						-- Same as the player
	knock_back = true,
	fear_height = 6,				-- Player takes damage from this height
	fall_damage = true,
	suffocation = 1,				-- Hit Points per second
	floats = 1,						-- Can swim
	reach = 4,						-- Nodes, same as player
	attack_type = 'dogfight',		-- Melee
	pathfinding = 1,				-- Enabled
	makes_footstep_sound = true,
	visual = 'mesh',
	visual_size = {x = 1, y = 1, z = 1},
	collisionbox = {-0.3, -1.0, -0.3, 0.3, 0.7, 0.3},
	selectionbox = {-0.3, -1.0, -0.3, 0.3, 0.7, 0.3},
	textures = {
		'mobs_test_character.png',		-- Skin
		'mobs_test_shield.png',			-- Armor overlay
		'default_tool_steelsword.png',	-- Right hand
		'mobs_test_transparent.png',	-- Can't remember
	},
	mesh = 'mobs_test_character.b3d',
	animation = {
		stand_start = 0,	stand_end = 80,		stand_speed = 30,
		walk_start = 168,	walk_end = 188,		walk_speed = 30,
		run_start = 168,	run_end = 188,		run_speed = 35,
		punch_start = 189,	punch_end = 199,	punch_speed = 30,
	},
})
